#SUJET 17#

#####EXERCICE 1#####

def RechercheMin(tab = []):
    if len(tab) == 1 :
        return 0
    else :
        mini = tab[0]
        indice_du_min = 0
        for i in range(len(tab)):
            if tab[i] < mini:
                mini = tab[i]
                indice_du_min = i
    return indice_du_min

def indice_du_min(tab=[]):
        return tab.index(min(tab))
        
#####EXERCICE 2#####    
    
def separe(tab):
    i = 0
    j = len(tab)-1
    while i < j :
        if tab[i] == 0 :
            i = i + 1
        else :
            tab[i], tab[j] = tab[j], tab[i]
            j = j - 1
    return tab