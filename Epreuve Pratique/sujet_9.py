#1
def moy(notes):
    m = 0
    #m: somme des notes multipliées par leurs coeff
    nm = 0
    #nm: somme des coefficients 
    for note in notes:
        n, c = note
        #affecte aux variables n et c les valeurs du tuple note
        nm += c
        m += n*c
    return m/nm

print(moy([(15,2),(9,1),(12,3)]))

#2
def pascal(n):
    C= [[1]]
    for k in range(1, n):
        Ck = [1]
        for i in range(1,k):
            Ck.append(C[k-1][i-1]+C[k-1][i])
        Ck.append(1)
        C.append(Ck)
    return C