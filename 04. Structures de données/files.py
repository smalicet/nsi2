class File:
    '''classe File
    création d’une instance File avec une liste
    '''
    def __init__(self):
        "Initialisation d’une file vide"
        self.structure = []
    def vide(self):
        "teste si la file est vide"
        return self.structure == []
    def enfiler(self,x):
        "enfile"
        self.structure.append(x)
    def defiler(self):
        "défile"
        return self.structure.pop(0)
    def taille(self):
        return len(self.structure)
    def __str__(self):
        mot = '<-- '
        for elt in self.structure:
            mot = mot + str(elt) + ' <-- '
        return mot
    def sommet(self):
        return self.structure[0]


def croisement(f1,f2):
    f3 = File()
    while not f1.vide() or not f2.vide():
        # Question 5 :
        if f1.vide():
            while not f2.vide():
                x = f2.defiler()
                f3.enfiler(x)
        elif f2.vide():
            while not f1.vide():
                x = f1.defiler()
                f3.enfiler(x)
        # Question 1 :
        elif f1.sommet() == 0 and f2.sommet() == 0:
            f1.defiler()
            f2.defiler()
            f3.enfiler(0)
        # Question 2 :
        elif f1.sommet() == 1 and f2.sommet() == 2:
            x = f1.defiler()
            f3.enfiler(x)
        # Question 3 :
        elif f1.sommet() == 1 and f2.sommet() == 0:
            x = f1.defiler()
            f3.enfiler(x)
            f2.defiler()
        # Question 4 :
        elif f1.sommet() == 0 and f2.sommet() == 2:
            x = f2.defiler()
            f3.enfiler(x)
            f1.defiler()
        
    return f3
                
f1 = File()
f1.enfiler(0)
f1.enfiler(1)
f1.enfiler(1)
f1.enfiler(0)
f1.enfiler(1)
f2 = File()
f2.enfiler(0)
f2.enfiler(2)
f2.enfiler(2)
f2.enfiler(2)
f2.enfiler(0)
f2.enfiler(2)
f2.enfiler(0)

print(f1)
print(f2)
print(croisement(f1,f2))

            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            