from matplotlib.pyplot import *

def ndep(e):
    if e==0:
        return 0
    else:
        return 4*ndep(e-1)+4



#def rotation(t):
#    dim = len(t)
#    if dim == 1:
#        return t
#    else:
#        A = rotation([[t[i][j] for j in range(dim//2)] for i in range(dim//2)])
#        B = rotation([[t[i][j] for j in range(dim//2,dim)] for i in range(dim//2)])
#        C = rotation([[t[i][j] for j in range(dim//2,dim)] for i in range(dim//2,dim)])
#        D = rotation([[t[i][j] for j in range(dim//2)] for i in range(dim//2,dim)])
#        t2 = []
#        for i in range(dim//2):
#            t2.append([D[i][j] for j in range(dim//2)]+[A[i][j] for j in range(dim//2)])
#        for i in range(dim//2):
#            t2.append([C[i][j] for j in range(dim//2)]+[B[i][j] for j in range(dim//2)])
#        return t2

#image = imread('lenna.png')
#imshow(rotation(image),cmap='gray')
#show()