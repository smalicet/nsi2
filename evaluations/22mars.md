# Maximum

Écrire la fonction `maximum` :

* prenant en paramètre une liste non vide de nombres : `nombres`
  
* renvoyant le plus grand élément de cette liste.

### Contrainte
On interdit d'utiliser `max`

### Exemples :

```python
>>> maximum([98, 12, 104, 23, 131, 9])
131
>>> maximum([-27, 24, -3, 15])
24
```

# Occurrences d'un caractère dans un mot

Écrire une fonction `compte_occurrences` qui prend en paramètres `caractere`, un caractère (une chaine de caractères de longueur 1), et `mot`, une chaine de caractères, et qui renvoie le nombre d'occurrences de `caractere` dans `mot`, c'est-à-dire le nombre de fois où `caractere` apparait dans `mot`.

> On n'utilisera pas la méthode `count`.

### Exemples

```python
>>> compte_occurrences("e", "sciences")
2
>>> compte_occurrences("i", "mississippi")
4
>>> compte_occurrences("a", "mississippi")
0
```



# Indice de la première occurrence

Écrire une fonction `indice` qui prend en paramètres `element` un nombre entier, `tableau` un tableau de nombres entiers, et qui renvoie l'indice de la première occurrence de `element` dans `tableau`.
La fonction devra renvoyer `None` si `element` est absent de `tableau`.

On n'utilisera pas ni la fonction `index`, ni la fonction `find`.

### Exemples

```python
>>> indice(1, [10, 12, 1, 56])
2
>>> indice(1, [1, 50, 1])
0
>>> indice(15, [8, 9, 10, 15])
3
>>> indice(1, [2, 3, 4]) is None
True
```



