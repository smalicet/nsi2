import module_arbre

########interface
    
def proposer(proposition):
    '''
    renvoie 'o' ou 'n' à la proposition formulée
    : param proposition (str)
    : return (str)
    '''
    pass
    

def afficher_victoire():
    '''
    affiche un message de victoire 
    '''
    pass
   


def rejouer_ou_pas() :
    '''
    propose de rejouer (utiliser la fct jouer) et relance la partie le cas échéant
    '''
    pass
    

def afficher_hibou(nb_animaux, phrase) :
    '''
    affiche le hibou, le nombre d'animaux qu'il connait dans l'arbre en cours
    et la phrase.
    : params
        nb_animaux (int)
        phrase (str)
    '''
    pass
    
    
    


def definir_animal() :
    '''
    renvoie une question permettant de retrouver un nouvel animal
    en demandant l'une et l'autre à l'utilisateur
    : return (tuple of str) (question, animal)
    '''
    pass
    

