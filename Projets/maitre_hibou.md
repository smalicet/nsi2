# Maître Hibou

## Apprendre sur un arbre 

```txt
  _^_____^__        (Je connais 79 animaux
 / ___  ___ \     ° ( est-ce que votre animal est un mammifère ?
/ / @ \/ @ \ \  °
\ \___/\___/ / 
 \____\/____/ 
 /     /\\\\\
 |     |\\\\\
  \     \\\\\\\
   \______/\\\\\
  ___||_||___  \\
  ---00-00--- 
```

Maître Hibou peut tenter de retrouver l'animal auquel vous pensez en vous posant des questions et, s'il ne le connaît pas, apprendre le nom et une caractéristique de cet animal.



Au départ, le hibou ne sait rien du tout. Il dispose d'une structure de donnée de type `Arbre` qui est vide.

```txt
        ∆
       / \
      ∆   ∆ 
```

Il suit alors l'algorithme suivant ....

Si l'arbre est vide,  Il vous demande :

* de proposer une question dont la réponse est oui ou non et qui décrit un animal. Par exemple :  **Est-ce que c'est un animal de compagnie ?"**.
* de lui donner l'animal auquel vous pensez et qui correspond à cette question. Par exemple, **un chien**.

Il construit alors l'arbre ci-dessous et l'insère à la place de l'arbre vide :

```txt
                   Est-ce que c'est un animal de compagnie ?
                             /oui                  \non
                           un chien                 ∆
                          /oui    \non
                         ∆         ∆
```
Sinon si l'arbre n'est pas vide :

* si le sous arbre gauche est vide, l'étiquette est le nom d'un animal(ci dessus, `un chien`). Le hibou vous propose alors ce nom et vous demande s'il est le bon :
    * si oui il a trouvé !
    * si non, il  se place dans le sous-arbre droit et recommence son algorithme.
* sinon, l'étiquette est une question et le hibou la pose :
    * si la réponse est oui,  il  se place dans le sous-arbre gauche et recommence son algorithme.
    * si non,  il  se place dans le sous-arbre droit et recommence son algorithme.


Petit à petit, Maître Hibou augmente ses connaissances et sera capable de deviner de plus en plus d'animaux.

```txt
          Est-ce  un animal de compagnie ?
     /oui                                   \non
   un chien  	                      Est-ce que il vit dans la jungle ?
 /oui       \non                           /oui               \non
∆    A-t-il a de grandes oreilles ?     un lion                 etc ....
         /oui                \non
       un lapin               etc ...
       /oui
      ∆  
```

D'ailleurs, avant de poser une question à l'utilisateur, il ne manque pas de lui préciser combien il connait d'animaux qui correspondent à la description que l'utilisateur lui a faite jusque là.

## Ça c'est cadeau

Je vous fournis deux modules `module_lineaire` qui gère les structures linéaires et  `module_arbre` avec deux méthodes intéressantes :

* `Arbre.charger(nom_fichier)` renvoie un arbre à partir des données du fichier texte passé en paramètre. 
* `sauvegarder(self, nom_fichier)` sauvegarde l'instance d'`Arbre` dans le fichier `nom_fichier`.

Vous pouvez toujours y jeter un coup d'oeil ... on y utilise une pile de parenthèses.

Et surtout n'oubliez pas de les télécharger et de les inclure dans votre dossier `projet_hibou_nom1_nom2`.



## Les tâches 

Pour mener à bien ce projet vous aurez besoin, au minimum (on peut/doit réaliser de petites fonctions annexes), des fonctions :

* `jouer()` : 
    * Charge l'arbre à partir d'un fichier associé ou, s'il n'y pas de fichier crée un `Arbre` vide
    * Lance le déroulement du jeu en se basant sur l'`Arbre` précédent.
    * Sauvegarde l'`Arbre`à l'issue de la partie dans le fichier associé.
    * Propose de rejouer .. ou pas et éventuellement relance une partie.
* `compter_animaux(arbre)` , une fonction récusive, renvoie le nombre d'animaux présents dans l'arbre.
* `derouler(arbre)` , une fonction récursive : déroule le jeu e se basant sur l'`Arbre` passé en paramètre.
* `ajouter_dans_arbre(arbre, question, animal)` qui ajoutera dans l'`Arbre` la `question` (str) et l'`animal` (str) passés en paramètre aux bons endroits.
* D'une interface utilisateur (composée de plusieurs fonctions) pour toutes les interactions entre lui et le hibou.



On respectera les principes de programmation habituels, entre autres  :

* Docstrings détaillées et précises (Difficile de faire des Doctests dans ce projet ...).
* Une fonction réalise une et une seule chose. Si elle en fait plusieurs, c'est qu'on a besoin de sous fonctions.
* Le même code ne doit pas apparaître à des endroits différents : on fait une fonction avec ce code et on l'appelle plusieurs fois.
* Les lignes de codes sont commentées.
* Séparation des algorithmes et de l'interface utilisateur  : aucun `print` et aucun `input` ailleurs que dans une fonction d'interface !
* On nomme fonctions et variables avec des noms désignant ce à quoi elles servent.



## Prolongements possibles :
* Ajouter une interface graphique plus agréable.
* Créer une base de données  pour jouer (le fichier texte) optimisée qui permette de trouver de façon très rapide n'importe quel animal.


C'est parti !
