import module_lineaire

class Arbre():
    '''
    une classe pour implémenter des arbres de façon récursive
    '''
    def __init__(self, etiquette = None, gauche = None, droit = None):
        '''
        construit un arbre vide avec un sous_arbre gauche et un sous arbre droit vides
        '''
        self.etiquette = etiquette
        self.sag = gauche
        self.sad = droit
        
    def est_vide(self):
        '''
        renvoie True si l'arbre est vide et False sinon
        : return (boolean)
        >>> a = Arbre()
        >>> a.est_vide()
        True
        '''
        return self.etiquette == None
        
    def renvoyer_sag(self):
        '''
        renvoie le fils gauche de l'arbre
        : return 
        type Arbre
        '''
        if self.sag == None :
            return Arbre()
        else :
            return self.sag

    def renvoyer_sad(self):
        '''
        renvoie le fils droit de l'arbre
        : return 
        type Arbre
        '''
        if self.sad == None :
            return Arbre()
        else :
            return self.sad
    
    def __str__(self):
        '''
        renvoie une chaîne de caractère représentant l'arbre
        '''
        if self.est_vide() :
            return '∆'
        else :
            return ( '('
                     + str(self.etiquette)
                     + ', '
                     + self.renvoyer_sag().__str__()        
                     + ', '
                     + self.renvoyer_sad().__str__()
                     +')'
                    )
    def taille(self) :
        '''
        renvoie la taille de l'Arbre
        : return (int)
        >>> a1.taille()
        5
        >>> f1.taille()
        1
        >>> a3.taille()
        3
        '''
        if self.est_vide() :
            return 0
        else :
            return 1 + self.renvoyer_sag().taille() + self.renvoyer_sad().taille()
    
    def hauteur(self) :
        '''
        renvoie la hauteur de l'arbre. l'arbre vide mesure -1 de haut
        : return (int)
        >>> a3.hauteur()
        1
        >>> f1.hauteur()
        0
        >>> a1.hauteur()
        2
        >>> Arbre().hauteur()
        -1
        '''
        if self.est_vide() :
            return -1
        else :
            return 1 + max(self.renvoyer_sag().hauteur(), self.renvoyer_sad().hauteur())
            
    def __repr__(self) :
        return self.__str__()
        
    def prefixe(self):
        '''
        renvoie une liste des étiquettes selon le parcours préfixe
        : return (list)
        >>> a1.prefixe()
        [1, 7, 3, 2, 1]
        '''
        if self.est_vide() :
            return []
        else :
            return ( [self.etiquette] 
                    + self.renvoyer_sag().prefixe()
                     + self.renvoyer_sad().prefixe()
                    )
    def infixe(self):
        '''
        renvoie une liste des étiquettes selon le parcours infixe
        : return (list)
        >>> a1.infixe()
        [7, 1, 2, 3, 1]
        '''
        if self.est_vide() :
            return []
        else :
            return ( self.renvoyer_sag().infixe()
                    + [self.etiquette]
                     + self.renvoyer_sad().infixe()
                    )
    def suffixe(self):
        '''
        renvoie une liste des étiquettes selon le parcours suffixe
        : return (list)
        >>> a1.suffixe()
        [7, 2, 1, 3, 1]
        '''
        if self.est_vide() :
            return []
        else :
            return ( self.renvoyer_sag().suffixe()
                    + self.renvoyer_sad().suffixe()
                    + [self.etiquette]
                   
                    )
        
    def largeur(self) :
        '''
        renvoie une liste des étiquettes parcourues en largeur
        : return (list)
        >>> a1.largeur()
        [1, 7, 3, 2, 1]
        '''
        file_arbres = module_lineaire.File()
        liste_etiq = []
        file_arbres.enfiler(self)
        while not file_arbres.est_vide() :
            arbre = file_arbres.defiler()
            if not arbre.est_vide() :
                liste_etiq.append(arbre.etiquette)
                file_arbres.enfiler(arbre.renvoyer_sag())
                file_arbres.enfiler(arbre.renvoyer_sad())
        return liste_etiq
 
    ### sauvegarder un arbre
    
      
    
    def sauvegarder(self, nom_fichier):
        '''
        sauvegarde l'arbre en mode texte dans mon_fichier
        : param mon_fichier (str)
        : pas de return
        '''
        assert type(nom_fichier) is str,'nom_fichier doit être du type str'
        chaine = self.__str__()
        chaine = chaine.replace(', ' , ',')
        try :
            ecriture = open(nom_fichier,'w',encoding='utf_8') 
        except FileNotFoundError :
            raise 
        ecriture.write(chaine) 

        
        
    ### charger un arbre
    def extraire_racine(chaine) :
        '''
        renvoie la racine de la chaine représentant un arbre
        et la chaine privée de cette racine
        : param (str)
        : return (tuple of str)
        >>> Arbre.extraire_racine('8,(3,(1, ∆, ∆),(6,∆, ∆)),(2,(7, ∆, ∆),∆)')
        ('8', '(3,(1, ∆, ∆),(6,∆, ∆)),(2,(7, ∆, ∆),∆)')
        '''
        racine = ''
        i = 0
        while chaine[i] != ',' :
            i = i + 1
        return chaine [:i], chaine [i + 1:]
        
    def extraire_sous_arbre(chaine) :
        '''
        renvoie le premier sous-arbre contenu dans la chaine
        et la chaine privée de ce sous-arbre
        :param chaine (str)
        : return (tuple of str)
        >>> Arbre.extraire_sous_arbre('(3,(1, ∆, ∆),(6,∆, ∆)),(2,(7, ∆, ∆),∆)')
        ('(3,(1, ∆, ∆),(6,∆, ∆))', '(2,(7, ∆, ∆),∆)')
        '''
        if chaine[0] == '∆' :
            return '∆', chaine[2:]
        
        else :
            i = 1
            sous_arbre = '('
            parenthese = module_lineaire.Pile()
            parenthese.empiler('(')
            while not parenthese.est_vide() :
                sous_arbre += chaine[i]
                if chaine[i] == '(' :
                    parenthese.empiler('(')
                elif chaine[i] == ')' :
                    parenthese.depiler()
                i = i + 1
            return sous_arbre, chaine [i + 1:]
    
    def construire(self, chaine):
        '''
        construit un arbre correspondant à la chaine passée en paramètre
        de façon récursive.
        :param chaine(str)
        : pas de return
        : effet de bord sur self
        >>> a = Arbre()
        >>> a.construire('(8,(3,(1,∆,∆),(6,(4,∆,∆),(7,∆,∆))),(10,∆,(14,(13,∆,∆),∆)))')
        >>> print(a)
        (8, (3, (1, ∆, ∆), (6, (4, ∆, ∆), (7, ∆, ∆))), (10, ∆, (14, (13, ∆, ∆), ∆)))
        '''
        # prépare la syntaxe de la chaine
        if chaine[0] == '(' :
            chaine = chaine[1:]
        if chaine[-1] == ')' :
            chaine = chaine[: -1]
        # extraire racine, sag et sad
        racine, chaine = Arbre.extraire_racine(chaine)
        sag, chaine = Arbre.extraire_sous_arbre(chaine)
        sad, chaine = Arbre.extraire_sous_arbre(chaine)
        self.etiquette = racine
        if sag != '∆' :
            arbre_gauche = Arbre()
            arbre_gauche.construire(sag)
            self.sag = arbre_gauche
        else :
            self.sag = Arbre()
        if sad != '∆':
            arbre_droit = Arbre()
            arbre_droit.construire(sad)
            self.sad = arbre_droit
        else:
            self.sad = Arbre()
            
    def charger(nom_fichier):
        '''
        renvoie un arbre construit à partir des données du fichier txt nom_ficher
        : param nom_fichier (str)
        : return (Arbre)
        '''
        assert type(nom_fichier) is str,'nom_fichier doit être du type str'
        try :
            lecture = open(nom_fichier,'r',encoding='utf_8') 
        except FileNotFoundError :
            raise 
        chaine = lecture.read()
        a = Arbre()
        a.construire(chaine)
        return a


        
class ABR(Arbre) :
    '''
    une classe pour implémenter des arbres binaire de recherche de façon récursive
    '''
    def __init__(self):
        '''
        construit un arbre vide avec un sous_arbre gauche et un sous arbre droit vides
        '''
        # on fait appelle au constructeur de la classe parente, la classe Arbre pour construire un ABR vide :
        super().__init__()
        
    def __repr__(self) :
        return ('Un ABR de taille '
                + str(self.taille())
                + ' est de hauteur '
                + str(self.hauteur())
                )
    
    def comp_defaut(a, b):
        '''
        renvoie -1 si a < b, 1 si a > b et 0 si a == b.
        : param a et b
        type comparables avec l'opérateur < ou >
        : return 0, -1 ou 1
        type int
        '''
        if a < b :
            return -1
        elif a > b :
            return 1
        else :
            return 0
        
    def inserer(self, cle, comp = comp_defaut):
        '''
        insérer une clé sa place dans l'ABR si celle-ci n'y est pas déjà
        : param 
           cles (? ou list de clé)
           comp (function)le comparateur utilisé
        : pas de return mais EFFET DE BORD SUR self
        >>> a = ABR()
        >>> a.inserer(8)
        >>> print(a)
        (8, ∆, ∆)
        >>> a.inserer(3)
        >>> a.inserer(1)
        >>> a.inserer(6)
        >>> a.inserer(4)
        >>> a.inserer(7)
        >>> a.inserer(10)
        >>> a.inserer(14)
        >>> a.inserer(13)
        >>> print(a)
        (8, (3, (1, ∆, ∆), (6, (4, ∆, ∆), (7, ∆, ∆))), (10, ∆, (14, (13, ∆, ∆), ∆)))
        '''
        if self.est_vide() :
            self.etiquette = cle
            self.sag = ABR()
            self.sad = ABR()
        elif comp(cle, self.etiquette) == -1 :                
            self.renvoyer_sag().inserer(cle, comp)
        elif comp(cle, self.etiquette) == 1 :
            self.renvoyer_sad().inserer(cle, comp)
        else :
            raise "clé déjà présente dans l'ABR"
        
    def rechercher(self, cle, comp = comp_defaut) :
        '''
        Renvoie True si la clé est presente et False sinon
        : param cle (?)
        : return (boolean)
        >>> a = ABR()
        >>> a.inserer(6)
        >>> a.inserer(13)
        >>> a.inserer(3)
        >>> a.rechercher(6)
        True
        >>> a.rechercher(13)
        True
        >>> a.rechercher(3)
        True
        >>> a.rechercher(9)
        False
        '''
        if self.etiquette == cle :
            return True
        elif self.est_vide() :
            return False
        elif comp(cle, self.etiquette) == -1 :
            return self.renvoyer_sag().rechercher(cle)
        else :
            return self.renvoyer_sad().rechercher(cle)


    
    
    
    
        
    
f2 = Arbre(2)
f1 = Arbre(1)
f7 = Arbre(7)
a3 = Arbre(3, f2, f1)
a1 = Arbre(1, f7, a3)       
        
    
            
           
        
################DOCTEST
        
if __name__ == '__main__' :
    f2 = Arbre(2)
    f1 = Arbre(1)
    f7 = Arbre(7)
    a3 = Arbre(3, f2, f1)
    a1 = Arbre(1, f7, a3)    
    import doctest
    doctest.testmod(verbose = True)
    
